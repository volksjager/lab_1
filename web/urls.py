from django.urls import path
from web.views import *

app_name = 'web'

urlpatterns = [
    path('', index, name='index'),
    path('add_rss', add_rss, name='add_rss'),
    path('delete_rss', delete_rss, name='delete_rss'),
    path('get_rss', get_rss, name='get_rss'),
     path('export_rss', export_rss, name='export_rss'),
]