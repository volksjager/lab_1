import feedparser
import json
import schedule
from threading import Thread
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from time import sleep

from web.models import Feed

def index(request):
    context = {
        "feeds": Feed.objects.all().order_by('-pk')
    }
    return render(request, 'index.html', context)

def add_rss(request):
    rss = request.POST.get('rss', None)
    feed = Feed.objects.get_or_create(url=rss)
    Thread(target=update_rss).start()
    return redirect('web:index')

@csrf_exempt
def delete_rss(request):
    Feed.objects.filter(pk=request.POST.get('rss', None)).delete()
    Thread(target=update_rss).start()
    return redirect('web:index')

@csrf_exempt
def get_rss(request):
    f = open('merged_rss.json')
    data = json.load(f)
    f.close()

    return JsonResponse(data, safe=False)

def export_rss(request):
    f = open('merged_rss.json')
    json_object = json.load(f)

    response = HttpResponse(content=json_object, content_type='application/json')
    response['Content-Disposition'] = f'attachment; filename=rss_export.json'
    return response


def update_rss():
    print("upd")
    rss_list = list()
    urls = [feed.url for feed in Feed.objects.all().order_by('-pk')]
    for url in urls:
        NewsFeed = feedparser.parse(url)
        data = NewsFeed.entries

        rss_list.extend(data)

    with open('merged_rss.json', 'w') as outfile:
        json.dump(rss_list, outfile)

def run_scheduler():
    update_rss()
    schedule.every(5).minutes.do(update_rss)

    while True:
        try:
            schedule.run_pending()
        except Exception as ex:
            logger.exception(ex)
        sleep(1)

Thread(target=run_scheduler).start()